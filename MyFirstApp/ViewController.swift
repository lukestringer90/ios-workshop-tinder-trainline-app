//
//  ViewController.swift
//  MyFirstApp
//
//  Created by Luke Stringer on 09/11/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let frame = CGRect(x: 50, y: 50, width: 200, height: 200)
        let button = UIButton(frame: frame)
        button.setTitle("Push View Controller", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        view.addSubview(button)
        
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc func buttonTapped() {
        print("Thing")
        
        guard let next = storyboard?.instantiateViewController(withIdentifier: "viewController") else {
            print("No view controller matching ID")
            return
        }
        navigationController?.pushViewController(next, animated: true)
    }
}
