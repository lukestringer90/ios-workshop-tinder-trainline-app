//
//  Transaction.swift
//  MyFirstApp
//
//  Created by Luke Stringer on 10/11/2021.
//

import Foundation

struct Transaction {
    let companyName: String
    let time: String
    let amount: Float
}
