//
//  TransactionCell.swift
//  MyFirstApp
//
//  Created by Luke Stringer on 10/11/2021.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    
    func configure(with transaction: Transaction) {
        companyNameLabel.text = transaction.companyName
        timeLabel.text = transaction.time
        amountLabel.text = String(format: "£%.2f", transaction.amount)
    }

}
